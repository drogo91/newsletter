# newletter

L'installation et l'utilisation peut se faire via docker (fortement recommandé)

Vous pouvez retrouver des commandes dans le makefile pour l'installation.
La base de données s'installe avec des migrations.

Vous pouvez retrouver les conteneurs suivants :

- phpmyadmin : http://localhost:8080
- maildev : http://localhost:8081
- le site en local : http://localhost:8741
