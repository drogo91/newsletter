<?php

namespace App\Service;

use App\Entity\Newsletter;
use App\Entity\User;
use App\Repository\NewsletterRepository;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class SendMail
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendMail(User $user, string $content): void
    {
        $email = (new Email())
            ->from('newsletter@example.com')
            ->to($user->getEmail())
            ->subject('Newsletter')
            ->html($content);

        $this->mailer->send($email);
    }
}
