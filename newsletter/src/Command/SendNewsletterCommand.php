<?php

namespace App\Command;

use App\Entity\Newsletter;
use App\Entity\User;
use App\Repository\NewsletterRepository;
use App\Service\SendMail;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:send-newsletter',
    description: 'Send newsletter',
)]
class SendNewsletterCommand extends Command
{
    private NewsletterRepository $newsletterRepository;
    private SendMail $sendMail;

    public function __construct(NewsletterRepository $newsletterRepository, SendMail $sendMail)
    {
        $this->newsletterRepository = $newsletterRepository;
        $this->sendMail = $sendMail;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('id', InputArgument::REQUIRED, 'Id newsletter')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $idNewsletter = $input->getArgument('id');

        /** @var Newsletter | null $newsletter */
        $newsletter = $this->newsletterRepository->find($idNewsletter);

        if ($newsletter === null) {
            $io->note(sprintf('L\'id %s n\'existe pas dans la base de données', $idNewsletter));
            return Command::FAILURE;
        }

        /** @var User $user */
        foreach ($newsletter->getUser() as $user) {
            $urlUnsubscribe = $_ENV['UNSUBSCRIBE_MAILER'] . $user->getId() . '/' . $newsletter->getId();
            $listNewsletter = $_ENV['LIST_BY_USER_MAILER'] . $user->getId();

            $firstname = $user->getFirstname() === null ? ' ' : ' ' . $user->getFirstname() . ' ';
            $content = 'Bonjour' . $firstname
                . '<br> Vous recevez cet email car vous etes abonné à la newsletter '
                . $newsletter->getName()
                . ' <br> Ce n\'est pas du spam :)'
            . '<br> <a href="'.$urlUnsubscribe.'">Cliquez ici</a> pour vous désinscrire / <a href="'.$listNewsletter.'">Cliquez ici</a> pour modifier vos abonnements';

            $this->sendMail->sendMail($user, $content);

            $io->note(sprintf('Email envoyé à %s', $user->getEmail()));
        }

        $io->success('Les mails sont envoyés');

        return Command::SUCCESS;
    }
}
