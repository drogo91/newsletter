<?php

namespace App\Controller;

use App\Entity\Newsletter;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{

    private $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    #[Route('/user/{id}', name: 'app_user')]
    public function index(string $id): Response
    {
        /** @var User | null $user */
        $user = $this->doctrine->getRepository(User::class)->find($id);

        if ($user === null) {
            return $this->redirectToRoute('app_news_letter');
        }

        return $this->render('user/index.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route('/user-delete-newsletter/{idUser}/{idNewsletter}', name: 'app_user_delete_newsletter')]
    public function deleteNewsletter(string $idUser, string $idNewsletter): Response
    {
        /** @var User | null $user */
        $user = $this->doctrine->getRepository(User::class)->find($idUser);

        /** @var Newsletter | null $newsletter */
        $newsletter = $this->doctrine->getRepository(Newsletter::class)->find($idNewsletter);

        if ($user === null) {
            return $this->redirectToRoute('app_news_letter');
        }

        if ($newsletter === null) {
            return $this->redirectToRoute('app_news_letter');
        }

        $user->removeNewsletter($newsletter);

        $this->doctrine->getManager()->persist($user);
        $this->doctrine->getManager()->flush();

        return $this->render('user/confirm-delete.html.twig', [
            'user' => $user,
            'newsletter' => $newsletter,
        ]);
    }
}
