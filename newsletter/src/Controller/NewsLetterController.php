<?php

namespace App\Controller;

use App\Entity\Newsletter;

use App\Entity\User;
use App\Form\UserType;
use App\Service\SendMail;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class NewsLetterController extends AbstractController
{
    private $sendMail;

    public function __construct(SendMail $sendMail)
    {
        $this->sendMail = $sendMail;
    }

    #[Route('/', name: 'app_news_letter')]
    public function index(ManagerRegistry $doctrine): Response
    {
        return $this->render('newsletter/index.html.twig', [
            'newsletters' => $doctrine->getRepository(Newsletter::class)->findAll(),
            ]
        );
    }

    #[Route('/newsletter/register/{id}', name: 'app_register_news_letter')]
    public function addUser(
        Request $request,
        ManagerRegistry $doctrine,
        string $id
    ): Response {
        /** @var Newsletter $newsletter */
        $newsletter = $doctrine->getRepository(Newsletter::class)->find($id);
        if ($newsletter === null) {
            return $this->render('newsletter/index.html.twig', [
                    'newsletters' => $doctrine->getRepository(Newsletter::class)->findAll(),
                ]
            );
        }

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $userData = $form->getData();
            /** @var User $user */
            $user = $doctrine->getRepository(User::class)->findOneBy(['email' => $user->getEmail()]);

            $content = 'Vous avez été inscrit avec succès à la newsletter : ' . $newsletter->getName();

            if ($user === null) {
                $userData->addNewsletter($newsletter);
                $doctrine->getManager()->persist($userData);
                $this->sendMail->sendMail($userData, $content);
            } else {
                $user->addNewsletter($newsletter);
                $doctrine->getManager()->persist($user);
                $this->sendMail->sendMail($user, $content);
            }

            $doctrine->getManager()->flush();

            return $this->render('newsletter/confirm.html.twig', []);
        }

        return $this->render('newsletter/register.html.twig', [
                'newsletter' => $newsletter,
                'form' => $form,
            ]
        );
    }
}
